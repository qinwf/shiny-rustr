## Translation

#### 1. Create a folder 

Create a folder with name of a language. For example `English -> en`.

Read http://www.w3schools.com/tags/ref_language_codes.asp to find the tag of the language.

#### 2. Update `new_folder/translation.json`

You can checkout `./en/translation.json` for example.


```json
{
    "nav": {
        "headr": "Rust and R Integration",
        "book": "Book",
        "..."
    },
    "headr": {
        "h2": "Rust and R Integration",
        "tt1": "is a Rust library that provides a Rust API to work with R. ",
        "..."
    },
    "fea": {
        "more": "More Info",
		"..."
    }
}

```

#### 3. Update `index.html`

Look for this part:

```html
<ul class="dropdown-menu">
    <li class=""><a href="#" onclick="i18obj.changeLanguage('zh-cn',function(){$('body').localize();});">简体中文</a></li>
    <li class=""><a href="#" onclick="i18obj.changeLanguage('zh-tw',function(){$('body').localize();})">繁体中文</a></li>
    <li class=""><a href="#" onclick="i18obj.changeLanguage('en',function(){$('body').localize();});">English</a></li>
    <li class="divider"></li>
    <li class=""><a href="https://github.com/rustr/rustr.github.io/tree/master/locales">Help Translate</a></li>
</ul>
```
